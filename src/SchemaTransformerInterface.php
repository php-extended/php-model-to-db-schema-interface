<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-to-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use PhpExtended\DbSchema\SchemaInterface;
use RuntimeException;
use Stringable;

/**
 * SchemaTransformerInterface interface file.
 * 
 * This interface represents a bridge between the schema objects as models, and
 * the schema objects as data structure in a RDBMS.
 * 
 * @author Anastaszor
 */
interface SchemaTransformerInterface extends Stringable
{
	
	/**
	 * Transforms the model schema into a real database schema.
	 * 
	 * @param ModelSchemaInterface $model
	 * @return SchemaInterface
	 * @throws RuntimeException if the transformation is not possible
	 */
	public function transformSchema(ModelSchemaInterface $model) : SchemaInterface;
	
}
