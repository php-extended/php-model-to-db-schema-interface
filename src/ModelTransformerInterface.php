<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-to-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use Iterator;
use PhpExtended\DbSchema\TableInterface;
use RuntimeException;
use Stringable;

/**
 * ModelTransformerInterface interface file.
 * 
 * This interface represents a bridge between the model objects as models, and
 * the table objects as data structure in a RDBMS.
 * 
 * @author Anastaszor
 */
interface ModelTransformerInterface extends Stringable
{
	
	/**
	 * Transforms the model objects into real database tables.
	 * 
	 * @param ModelObjectInterface $model
	 * @return Iterator<TableInterface>
	 * @throws RuntimeException if the transformation is not possible
	 */
	public function transformModelObject(ModelObjectInterface $model) : Iterator;
	
}
