<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-to-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use PhpExtended\DbSchema\DatabaseInterface;
use RuntimeException;
use Stringable;

/**
 * DatabaseTransformerInterface interface file.
 * 
 * This interface represents a bridge between the database objects as models,
 * and the database objects as data structure in a RDBMS.
 * 
 * @author Anastaszor
 */
interface DatabaseTransformerInterface extends Stringable
{
	
	/**
	 * Transforms the model database into a real database schema. This MUST
	 * return a correct database object, MUST NOT return null, and MUST throw
	 * an exception if this is not possible.
	 * 
	 * @param ModelDatabaseInterface $model
	 * @return DatabaseInterface
	 * @throws RuntimeException if the transformation is not possible
	 */
	public function transformDatabase(ModelDatabaseInterface $model) : DatabaseInterface;
	
}
