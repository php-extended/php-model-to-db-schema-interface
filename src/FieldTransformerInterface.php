<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-to-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use PhpExtended\DbSchema\ColumnDatetimeInterface;
use PhpExtended\DbSchema\ColumnJsonInterface;
use PhpExtended\DbSchema\ColumnNumberInterface;
use PhpExtended\DbSchema\ColumnSpatialInterface;
use PhpExtended\DbSchema\ColumnStringInterface;
use PhpExtended\DbSchema\ColumnTextInterface;
use RuntimeException;
use Stringable;

/**
 * FieldTransformerInterface interface file.
 * 
 * This interface represents a bridge between the model fields as models, and
 * the table columns as data structure in a RDBMS.
 * 
 * @author Anastaszor
 */
interface FieldTransformerInterface extends Stringable
{
	
	/**
	 * Transforms the blob field into a real text table column.
	 * 
	 * @param ModelFieldBlobInterface $field
	 * @return ColumnTextInterface
	 * @throws RuntimeException if the transformation is not possible
	 */
	public function transformBlobField(ModelFieldBlobInterface $field) : ColumnTextInterface;
	
	/**
	 * Transforms the boolean field into a real number table column.
	 * 
	 * @param ModelFieldBooleanInterface $field
	 * @return ColumnNumberInterface
	 * @throws RuntimeException if the transformation is not possible
	 */
	public function transformBooleanField(ModelFieldBooleanInterface $field) : ColumnNumberInterface;
	
	/**
	 * Transforms the datetime field into a real spatial datetime column.
	 * 
	 * @param ModelFieldDateTimeInterface $field
	 * @return ColumnDatetimeInterface
	 * @throws RuntimeException if the transformation is not possible
	 */
	public function transformDateTimeField(ModelFieldDateTimeInterface $field) : ColumnDatetimeInterface;
	
	/**
	 * Transforms the float field into a real number table column.
	 * 
	 * @param ModelFieldFloatInterface $field
	 * @return ColumnNumberInterface
	 * @throws RuntimeException if the transformation is not possible
	 */
	public function transformFloatField(ModelFieldFloatInterface $field) : ColumnNumberInterface;
	
	/**
	 * Transforms the integer field into a real number table column.
	 * 
	 * @param ModelFieldIntegerInterface $field
	 * @return ColumnNumberInterface
	 * @throws RuntimeException if the transformation is not possible
	 */
	public function transformIntegerField(ModelFieldIntegerInterface $field) : ColumnNumberInterface;
	
	/**
	 * Transforms the json field into a real json table column.
	 * 
	 * @param ModelFieldJsonInterface $field
	 * @return ColumnJsonInterface
	 * @throws RuntimeException if the transformation is not possible
	 */
	public function transformJsonField(ModelFieldJsonInterface $field) : ColumnJsonInterface;
	
	/**
	 * Transforms the spatial field into a real spatial table column.
	 * 
	 * @param ModelFieldSpatialInterface $field
	 * @return ColumnSpatialInterface
	 * @throws RuntimeException if the transformation is not possible
	 */
	public function transformSpatialField(ModelFieldSpatialInterface $field) : ColumnSpatialInterface;
	
	/**
	 * Transforms the string field into a real string or text table column. 
	 * 
	 * @param ModelFieldStringInterface $field
	 * @return ColumnStringInterface
	 * @throws RuntimeException if the transformation is not possible
	 */
	public function transformStringField(ModelFieldStringInterface $field) : ColumnStringInterface;
	
}
